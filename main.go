package main

import (
	"archive/zip"
	"fmt"
	//	"io"
	"io/ioutil"
	"log"
	"strings"
)

type Image struct {
	thumbnail   string
	imageURL    string
	description string
}

func extractPreviewImageFromZip(archiveName string) {
	r, err := zip.OpenReader("./drawings/" + archiveName)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	for _, f := range r.File {
		//r, w := io.Pipe()
		if f.Name == "preview.png" {
			rc, err := f.Open()
			if err != nil {
				log.Fatal(err) // better ormatting of error description later
			}
			//			_, err = rc.Read(r)

			//generally not good to ReadAll from a reader interface, but we know the thumbnails are small.
			dataAsByteSlice, err := ioutil.ReadAll(rc)
			if err != nil {
				log.Fatal(err)
			}
			rc.Close()

			previewFileName := strings.TrimSuffix(archiveName, ".kra")
			err = ioutil.WriteFile(previewFileName+".png", dataAsByteSlice, 0600)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

}

func main() {
	files, err := ioutil.ReadDir("./drawings")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		fmt.Println(file.Name())
		if strings.Contains(file.Name(), ".kra") {
			extractPreviewImageFromZip(file.Name())
		}
	}

}
